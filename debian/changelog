python-isoweek (1.3.3-6) unstable; urgency=medium

  * QA upload.
  * Orphan the package.
  * debian/control: Remove myself from the uploaders list.

 -- Boyuan Yang <byang@debian.org>  Mon, 25 Mar 2024 16:11:20 -0400

python-isoweek (1.3.3-5) unstable; urgency=medium

  * Update uploaders list to remove retired maintainer.
    (Closes: #1050273)
  * debian/control:
    + Use debhelper compat v13.
    + Use Standards-Version 4.6.2.

 -- Boyuan Yang <byang@debian.org>  Mon, 25 Dec 2023 11:20:47 -0500

python-isoweek (1.3.3-4) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Sandro Tosi <morph@debian.org>  Fri, 03 Jun 2022 14:37:35 -0400

python-isoweek (1.3.3-3) unstable; urgency=medium

  * Team upload.
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.
  * Drop Python 2 support.

 -- Ondřej Nový <onovy@debian.org>  Wed, 31 Jul 2019 10:59:45 +0200

python-isoweek (1.3.3-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/watch: Use https protocol
  * d/changelog: Remove trailing whitespaces
  * d/control: Remove trailing whitespaces
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field

  [ Hugo Lefeuvre ]
  * debian/control:
    - Bump Standards-Version to 4.1.5.
    - Bump debhelper dependency to match new compat.
    - Add autopkgtest stanza.
  * Bump debian/compat to 11.
  * Update debian/copyright years.
  * Bump debian/watch to version 4.

 -- Hugo Lefeuvre <hle@debian.org>  Thu, 02 Aug 2018 15:37:46 +0800

python-isoweek (1.3.3-1) unstable; urgency=medium

  * Upload to unstable.
  * New upstream release.
  * debian/control:
    - Bump Standards-Version to 4.0.0.

 -- Hugo Lefeuvre <hle@debian.org>  Fri, 04 Aug 2017 17:28:40 +0200

python-isoweek (1.3.2-1) experimental; urgency=low

  * New upstream release.
  * Bump compat level to 10, update debhelper dependency accordingly.
  * debian/copyright:
    - Bump copyright years.
    - Update Hugo Lefeuvre's e-mail address.

 -- Hugo Lefeuvre <hle@debian.org>  Fri, 09 Jun 2017 19:40:36 +0200

python-isoweek (1.3.1-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    - Update Vcs-* URLs to match new repository.
    - Update Hugo Lefeuvre's e-mail address.
    - Bump Standards-Version to 3.9.8.
  * debian/copyright:
    - Update copyright years.
    - Update Format field to use HTTPS protocol.
    - Update Hugo Lefeuvre's e-mail address.

 -- Hugo Lefeuvre <hle@debian.org>  Sun, 18 Dec 2016 13:10:23 +0200

python-isoweek (1.3.0-2) unstable; urgency=low

  * debian/compat:
    - Bump compat level to 9.
  * debian/control:
    - Bump debhelper's minimal version to 9.
  * debian/copyright:
    - Update copyright years.
  * debian/watch:
    - Use pypi.debian.net instead of pypi.python.org, following
      DebianWiki's recommendation.

 -- Hugo Lefeuvre <hugo6390@orange.fr>  Wed, 15 Apr 2015 18:22:04 +0200

python-isoweek (1.3.0-1) unstable; urgency=low

  * Initial release (Closes: 771591).

 -- Hugo Lefeuvre <hugo6390@orange.fr>  Tue, 23 Dec 2014 12:09:20 +0100
